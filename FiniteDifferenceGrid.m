(* Package: FiniteDifferenceGrid *)
(* Description: Finite difference approximation grid. *)
(* Version: 0.1.0 *)
(* Author: Maciej Maliborski, maciej.maliborski@gmail.com *)

BeginPackage["FiniteDifferenceGrid`", {"Developer`"}];

(* Unprotect[]; *)
ClearAll[FiniteDifferenceGrid, FiniteDifferenceD, FiniteDifferenceGridFunctionUniform, generateDifferentiationMatrices];

(* Usage *)

FiniteDifferenceGrid::usage = "FiniteDifferenceGrid[n, {a, b}] \
creates FiniteDifferenceGridFunctionUniform object containing \
precomputed information for finite-difference apporximation of \
derivatives in one spatial dimension.";

(* FiniteDifferenceD::usage = "FiniteDifferenceD[g][k][f, {p_1, p_2}] \ *)
(* returns finite-difference approximation of k-th derivative of a *)
(* function with parities {p_1,p_2} represented by a vector f defined \ *)
(* on the grid g."; *)

FiniteDifferenceGridFunctionUniform::usage = "";

(* FiniteDifferenceDissipation::usage = *)
(* "FiniteDifferenceDissipation[g][f, {p_1, p_2}]."; *)


(* Warning messages *)

Begin["`Private`"];


Options[FiniteDifferenceGrid] = {"DifferenceOrder" -> 2, "Rank" -> 2,
				 "StaggerFractions" -> {0, 0},
				 "WorkingPrecision" -> Automatic};

FiniteDifferenceGrid[n_Integer, {a_?NumericQ, b_?NumericQ}, opts:OptionsPattern[]] :=
   Module[{p, rk, s, wp, np, h, xg, mg, q, dg, fopts = FilterRules[{opts}, Options[FiniteDifferenceGrid]], leftZero, rightZero},

	  p = OptionValue[FiniteDifferenceGrid, fopts, "DifferenceOrder"];

	  rk = OptionValue[FiniteDifferenceGrid, fopts, "Rank"];

	  s = OptionValue[FiniteDifferenceGrid, fopts, "StaggerFractions"];

	  wp = OptionValue[FiniteDifferenceGrid, fopts, "WorkingPrecision"] /. {Automatic -> MachinePrecision};

	  np = ToPackedArray[N[#, wp]]&;

	  h = (b-a)/(Total[s]+n-1);
	  xg = (a + (s[[1]]+(#-1))*h)&/@Range[n] // np;

	  (* Differentiation matrices *)
	  mg = Association[# -> generateDifferentiationMatrices[{xg, {a, b}, s}, rk, p, #] & /@ Tuples[{"Even", "Odd", "None"}, 2]];

	  (* Dissipation matrices *)
	  q = p + 2;
	  dg = Association[# -> (-1)^(q/2)*h^(q-1)*Last[generateDifferentiationMatrices[{xg, {a, b}, s}, q, 2, #]] & /@ Tuples[{"Even", "Odd", "None"}, 2]];
	  leftZero = SparseArray@ReplacePart[#, Table[i -> SparseArray[{}, n], {i, 1, 1 + p/2}]] &;
	  rightZero = SparseArray@ReplacePart[#, Table[i -> SparseArray[{}, n], {i, n - p/2, n}]] &;

	  dg = Append[dg, Flatten[{{{"Zero", #} -> leftZero[{"Even", #} /. dg], {#, "Zero"} -> rightZero[{#, "Even"} /. dg]} & /@ {"Even", "Odd", "None"},
				   {"Zero", "Zero"} -> rightZero@leftZero[{"None", "None"} /. dg]}]];

	  FiniteDifferenceGridFunctionUniform[xg, mg, dg, {}, {n, np@h, {a, b}, s, Total[s]!=0}]
   ];

(* FiniteDifferenceGrid[n_Integer, {x1_?NumericQ, x2_?NumericQ}, OptionsPattern[]] := *)
(*    Module[{ng = n, hg, xg, mg, dg, *)
(* 	   rk = OptionValue["Rank"], p = OptionValue["DifferenceOrder"], *)
(* 	   sym = OptionValue["Symmetries"], q}, *)

(* 	  (\* Uniform grid *\) *)
(* 	  hg = (x2-x1)/(ng-1); *)
(* 	  xg = Range[x1, x2, hg]; *)

(* 	  (\* Differentiation matrices *\) *)
(* 	  mg = generateDifferentiationMatrices[xg, rk, p, sym]; *)

(* 	  (\* Dissipation matrix *\) *)
(* 	  q = p + 2; *)
(* 	  dg = (-1)^(q/2)*hg^(q-1)*Last[generateDifferentiationMatrices[xg, q, 2, sym]]; *)

(* 	  FiniteDifferenceGridFunctionUniform[xg, mg, dg, {ng, hg, rk, p}] *)
(*    ]; *)

FiniteDifferenceGridFunctionUniform[__]["Methods"] := Sort@{"DifferentiationMatrix", "Methods", "Grid", "DifferenceOrder", "Rank", "DissipationMatrix", "Domain"};


FiniteDifferenceGridFunctionUniform[__, {_, _, {a_,b_}, __}]["Domain"] := {a,b};

FiniteDifferenceGridFunctionUniform[xg_, __]["Grid"] := xg;

FiniteDifferenceGridFunctionUniform[_, mg_, __]["DifferentiationMatrix"][k_Integer, {p1_String, p2_String}] := Part[{p1, p2} /. mg, k];

FiniteDifferenceGridFunctionUniform[_, _, dg_, __]["DissipationMatrix"][{p1_String, p2_String}] := {p1, p2} /. dg;

(* FiniteDifferenceD[g_[xg_, mg_, ___]][k_Integer][f_?VectorQ] := mg[[k]].f; *)

(* FiniteDifferenceDissipation[g_[xg_, mg_, dg_, ___]][f_?VectorQ] := dg.f; *)

(* generateDifferentiationMatrices[xg_?VectorQ, {a_?NumericQ, b_?NumericQ}, rk_Integer, ord_Integer, {s1_String, s2_String}] := *)

(*   Module[{nx = Length[xg], yg, f, fyg, d, p1, p2, imin, imax}, *)
(*    yg = Join[ *)
(*      Switch[s1, "None", {}, "Even" | "Odd", *)
(*       Table[2*a - xg[[1 + k]], {k, nx - 1, 1, -1}], _, *)
(*       Message[generateDifferentiationMatrices::symunk, s1]], xg, *)
(*      Switch[s2, "None", {}, "Even" | "Odd", *)
(*       Table[2*b - xg[[nx - k]], {k, 1, nx - 1, 1}], _, *)
(*       Message[generateDifferentiationMatrices::symunk, s1]]]; *)

(*    fyg = Join[ *)
(*      Switch[s1, "None", {}, "Even", *)
(*       Table[Subscript[f, 1] + (-1)^p1 (Subscript[f, 1 + k] - Subscript[f, 1]), {k, *)
(*          nx - 1, 1, -1}] /. {p1 -> 2}, "Odd", *)
(*       Table[Subscript[f, *)
(*          1] + (-1)^p1 (Subscript[f, 1 + k] - Subscript[f, 1]), {k, *)
(*          nx - 1, 1, -1}] /. {p1 -> 1}], *)
(*      Table[Subscript[f, k], {k, 1, nx}], *)
(*      Switch[s2, "None", {}, "Even", *)
(*       Table[Subscript[f, *)
(*          nx] - (-1)^p2 (Subscript[f, nx] - Subscript[f, nx - k]), {k, *)
(*          1, nx - 1, 1}] /. {p2 -> 2}, "Odd", *)
(*       Table[ *)
(*         Subscript[f, *)
(*          nx] - (-1)^p2 (Subscript[f, nx] - Subscript[f, nx - k]), {k, *)
(*          1, nx - 1, 1}] /. {p2 -> 1}]]; *)

(*    Do[d[i] = *)
(*      NDSolve`FiniteDifferenceDerivative[Derivative[i], yg, *)
(*       DifferenceOrder -> ord], {i, rk}]; *)

(*    imin = Switch[s1, "None", 1, "Even" | "Odd", nx]; *)
(*    imax = imin + nx - 1; *)

(*    Table[Last@ *)
(*      CoefficientArrays[d[i][fyg][[imin ;; imax]], *)
(*       fyg[[imin ;; imax]]], {i, rk}] *)

(*    ]; *)



generateDifferentiationMatrices[{xg_?VectorQ, {a_?NumericQ, b_?NumericQ},
				 {s1_?NumericQ, s2_?NumericQ}},
				rk_Integer, ord_Integer, {p1_String, p2_String}] :=
   Module[{nx = Length[xg], yg, f, fyg, d, pp1, pp2, imin, imax},

	 {pp1, pp2} = {p1, p2} /. {"Even" -> 2, "Odd" -> 1};

	 yg =
	 Join[
	    Switch[p1,
		   "None", {},
		   "Even" | "Odd", If[s1 == 0,
				      Table[2*a - xg[[1 + k]], {k, nx - 1, 1, -1}],
				      Table[2*a - xg[[-k+1]], {k, -nx+1, 0, 1}]
				   ],
		   _, Message[generateDifferentiationMatrices::symunk, p1]; Abort[];
	    ],
	    xg,
	    Switch[p2,
		   "None", {},
		   "Even" | "Odd", If[s2 == 0,
				      Table[2*b - xg[[nx - k]], {k, 1, nx - 1, 1}],
				      Table[2*b - xg[[2*nx-k+1]], {k, nx+1, 2*nx, 1}]
				   ],
		   _, Message[generateDifferentiationMatrices::symunk, p2]; Abort[];
	    ]
	 ];


	 fyg =
	 Join[
	    Switch[p1,
		   "None", {},
		   _, If[s1 == 0,
			 Table[Subscript[f,1]+(-1)^pp1*(Subscript[f,1+k]-Subscript[f,1]), {k,nx - 1, 1, -1}],
			 Table[(-1)^pp1*Subscript[f,-k+1], {k, -nx+1, 0, 1}]
		      ]
	    ],
	    Table[Subscript[f, k], {k, 1, nx}],
	    Switch[p2,
		   "None", {},
		   _, If[s2 == 0,
			 Table[Subscript[f,nx]-(-1)^pp2*(Subscript[f,nx]-Subscript[f,nx-k]), {k,1,nx-1,1}],
			 Table[(-1)^pp2*Subscript[f,2*nx-k+1], {k, nx+1, 2*nx, 1}]
		      ]
	    ]
	 ];


   Do[d[i] =
     NDSolve`FiniteDifferenceDerivative[Derivative[i], yg,
      DifferenceOrder -> ord], {i, rk}];

   imin = Switch[p1,
		 "None", 1,
		 "Even" | "Odd", If[s1 == 0, nx, nx+1]
	  ];
   imax = imin + nx - 1;

   Table[Last@
     CoefficientArrays[d[i][fyg][[imin ;; imax]],
      fyg[[imin ;; imax]]], {i, rk}]

   ];


(* Makeboxes[g_, fmt_] ^:= *)
(*    Module[{shown, hidden, icon, x = N@g["Grid"]}, *)
(* 	  shown = {{BoxForm`MakeSummaryItem[{"Domain: ", {First[x], Last[x]}}, fmt], SpanFromLeft}, *)
(* 		   {BoxForm`MakeSummaryItem[{"Size: ", Length@x}, fmt], SpanFromLeft}, *)
(* 		   {BoxForm`MakeSummaryItem[{"Rank: ", g["Rank"]}, fmt], SpanFromLeft} *)
(* 		  }; *)
(* 	  hidden = {{BoxForm`MakeSummaryItem[{"Coordinates: ", *)
(* 					      {{First[x], Last[x]}, {Min[Differences[x]], Max[Differences[x]]}}}, fmt], SpanFromLeft} *)
(*             }; *)

(* 	  icon = ListPlot[{1,2,3}, FrameTicks -> None, AspectRatio -> 1, ImageSize -> 25]; *)

(*    BoxForm`ArrangeSummaryBox[FiniteDifferenceGridFunctionUniform, x, icon, *)
(*                              shown, hidden, fmt, "Interpretable" -> False] *)
(*   ]; *)



End[];

SetAttributes[{FiniteDifferenceGrid,
	       FiniteDifferenceGridFunctionUniform}, {Protected,
	       ReadProtected}];

EndPackage[];
